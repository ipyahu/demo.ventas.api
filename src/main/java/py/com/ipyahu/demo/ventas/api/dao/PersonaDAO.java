package py.com.ipyahu.demo.ventas.api.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import py.com.ipyahu.demo.ventas.api.model.Persona;

public interface PersonaDAO extends JpaRepository<Persona, Integer>{

}
