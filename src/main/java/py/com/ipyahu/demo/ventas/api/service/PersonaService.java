package py.com.ipyahu.demo.ventas.api.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import py.com.ipyahu.demo.ventas.api.model.Persona;

public interface PersonaService extends CRUD<Persona>{

	Page<Persona> listarPaginado(Pageable paginacion);
}
