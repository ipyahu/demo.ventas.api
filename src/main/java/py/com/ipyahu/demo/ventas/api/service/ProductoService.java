package py.com.ipyahu.demo.ventas.api.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import py.com.ipyahu.demo.ventas.api.model.Producto;

public interface ProductoService extends CRUD<Producto> {
	
	Page<Producto> listarPaginado(Pageable paginado);

}
