package py.com.ipyahu.demo.ventas.api.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.ipyahu.demo.ventas.api.dao.PersonaDAO;
import py.com.ipyahu.demo.ventas.api.dao.ProductoDAO;
import py.com.ipyahu.demo.ventas.api.dao.VentaDAO;
import py.com.ipyahu.demo.ventas.api.model.Venta;
import py.com.ipyahu.demo.ventas.api.service.VentaService;

@Service
public class VentaServiceImpl implements VentaService{

	@Autowired
	private VentaDAO vDAO;
	
	@Autowired
	private ProductoDAO pDAO;
	
	@Autowired
	private PersonaDAO hDAO;
	
	@Transactional
	@Override
	public Venta registrar(Venta obj) {
		obj.getDetalleVenta().forEach(v -> v.setVent(obj));
		vDAO.save(obj);
		return obj;
	}

	@Override
	public Venta listarId(Integer id) {
		Venta venta = vDAO.findOne(id);
		venta.setPersona(hDAO.findOne(venta.getPersona().getIdPersona()));
		venta.getDetalleVenta().forEach(dv -> dv.setProducto(pDAO.findOne(dv.getProducto().getIdProducto())));
		return venta;
	}

	@Override
	public List<Venta> listar() {
		List<Venta> ventas = vDAO.findAll();
		ventas.forEach(v -> v.setPersona(hDAO.findOne(v.getPersona().getIdPersona())));
		ventas.forEach(v -> v.getDetalleVenta().forEach(dv -> dv.setProducto(pDAO.findOne(dv.getProducto().getIdProducto()))));
		return ventas;
	}

}
