package py.com.ipyahu.demo.ventas.api.service;

import java.util.List;

public interface CRUD<T>{

	T registrar(T obj);
	T modificar(T obj);
	void eliminar(Integer id);
	T listarId(Integer id);
	List<T> listar();
	
}
