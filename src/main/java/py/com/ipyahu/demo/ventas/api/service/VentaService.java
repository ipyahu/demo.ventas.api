package py.com.ipyahu.demo.ventas.api.service;

import java.util.List;

import py.com.ipyahu.demo.ventas.api.model.Venta;

public interface VentaService{

	Venta registrar(Venta obj);
	Venta listarId(Integer id);
	List<Venta> listar();
}
