package py.com.ipyahu.demo.ventas.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import py.com.ipyahu.demo.ventas.api.dao.ProductoDAO;
import py.com.ipyahu.demo.ventas.api.model.Producto;
import py.com.ipyahu.demo.ventas.api.service.ProductoService;

@Service
public class ProductoServiceImpl implements ProductoService{

	@Autowired
	private ProductoDAO dao;
	
	@Override
	public Producto registrar(Producto obj) {
		return dao.save(obj);
	}

	@Override
	public Producto modificar(Producto obj) {
		return dao.save(obj);
	}

	@Override
	public void eliminar(Integer id) {
		dao.delete(id);
	}

	@Override
	public Producto listarId(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public List<Producto> listar() {
		return dao.findAll();
	}

	@Override
	public Page<Producto> listarPaginado(Pageable paginado) {
		return dao.findAll(paginado);
	}
}
