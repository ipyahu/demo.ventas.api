package py.com.ipyahu.demo.ventas.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import py.com.ipyahu.demo.ventas.api.model.Persona;
import py.com.ipyahu.demo.ventas.api.service.PersonaService;

@Api(value="Servicio REST para personas")
@RestController
@RequestMapping("/personas")
public class PersonaController {

	@Autowired
	private PersonaService service;
	
	@GetMapping
	public ResponseEntity<List<Persona>> listar(){
		return new ResponseEntity<List<Persona>>(service.listar(), HttpStatus.OK);
	}
	
	@GetMapping(value="/paginado")
	public ResponseEntity<Page<Persona>> listarPaginado(Pageable paginacion){
		return new ResponseEntity<Page<Persona>>(service.listarPaginado(paginacion), HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<Persona> listarId(@PathVariable("id") Integer id){
		return new ResponseEntity<Persona>(service.listarId(id), HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Persona> registrar(@Valid @RequestBody Persona obj){
		return new ResponseEntity<Persona>(service.registrar(obj), HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<Persona> modificar(@Valid @RequestBody Persona obj){
		return new ResponseEntity<Persona>(service.modificar(obj), HttpStatus.OK);
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
}
