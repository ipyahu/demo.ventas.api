package py.com.ipyahu.demo.ventas.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import py.com.ipyahu.demo.ventas.api.dao.PersonaDAO;
import py.com.ipyahu.demo.ventas.api.model.Persona;
import py.com.ipyahu.demo.ventas.api.service.PersonaService;

@Service
public class PersonaServiceImpl implements PersonaService{

	@Autowired
	private PersonaDAO dao;
	
	@Override
	public Persona registrar(Persona obj) {
		return dao.save(obj);
	}

	@Override
	public Persona modificar(Persona obj) {
		return dao.save(obj);
	}

	@Override
	public void eliminar(Integer id) {
		dao.delete(id);
	}

	@Override
	public Persona listarId(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public List<Persona> listar() {
		return dao.findAll();
	}

	@Override
	public Page<Persona> listarPaginado(Pageable paginacion) {
		return dao.findAll(paginacion);
	}

}
