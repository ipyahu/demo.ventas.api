package py.com.ipyahu.demo.ventas.api.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import py.com.ipyahu.demo.ventas.api.model.DetalleVenta;

public interface DetalleVentaDAO extends JpaRepository<DetalleVenta, Integer>{

}
