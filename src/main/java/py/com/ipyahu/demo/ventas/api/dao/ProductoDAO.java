package py.com.ipyahu.demo.ventas.api.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import py.com.ipyahu.demo.ventas.api.model.Producto;

public interface ProductoDAO extends JpaRepository<Producto, Integer>{

}
