package py.com.ipyahu.demo.ventas.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import py.com.ipyahu.demo.ventas.api.model.Venta;
import py.com.ipyahu.demo.ventas.api.service.VentaService;

@Api(value="Servicios REST para ventas")
@RestController
@RequestMapping("/ventas")
public class VentaController {

	@Autowired
	private VentaService service;
	
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Venta>> listar(){
		List<Venta> ventas = new ArrayList<>();
		ventas = service.listar();
		return new ResponseEntity<List<Venta>>(ventas, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Venta> listarId(@PathVariable("id") Integer id){
		return new ResponseEntity<Venta>(service.listarId(id), HttpStatus.OK);
	}
	
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Venta> registrar(@RequestBody Venta obj){
		Venta vnt = service.registrar(obj);
		System.out.println(vnt.toString());
		return new ResponseEntity<Venta>(vnt, HttpStatus.CREATED);
	}
	
}
