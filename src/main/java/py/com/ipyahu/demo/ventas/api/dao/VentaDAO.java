package py.com.ipyahu.demo.ventas.api.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import py.com.ipyahu.demo.ventas.api.model.Venta;

public interface VentaDAO extends JpaRepository<Venta, Integer>{

}
