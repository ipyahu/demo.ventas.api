package py.com.ipyahu.demo.ventas.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import py.com.ipyahu.demo.ventas.api.model.Producto;
import py.com.ipyahu.demo.ventas.api.service.ProductoService;

@Api(value="Servicio REST para producto")
@RestController
@RequestMapping("/productos")
public class ProductoController {

	@Autowired
	private ProductoService service;
	
	@GetMapping
	public ResponseEntity<List<Producto>> listar(){
		return new ResponseEntity<List<Producto>>(service.listar(), HttpStatus.OK);
	}
	
	@GetMapping(value="/paginado")
	public ResponseEntity<Page<Producto>> listarPaginado(Pageable paginado){
		return new ResponseEntity<Page<Producto>>(service.listarPaginado(paginado), HttpStatus.OK);
	}	
	
	@GetMapping(value="/{id}")
	public ResponseEntity<Producto> listarId(@PathVariable("id") Integer id){
		return new ResponseEntity<Producto>(service.listarId(id), HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Producto> registrar(@Valid @RequestBody Producto obj){
		return new ResponseEntity<Producto>(service.registrar(obj), HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<Producto> modificar(@Valid @RequestBody Producto obj){
		return new ResponseEntity<Producto>(service.modificar(obj), HttpStatus.OK);
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
}
